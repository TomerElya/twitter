FROM golang:1.14

ENV GO111MODULE=on

WORKDIR /go/src/twitter

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

RUN go build -o twitterserver
CMD ["./twitterserver"]