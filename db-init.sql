SELECT 'CREATE DATABASE twitter
		WITH
		ENCODING = "UTF-8"
		LC_COLLATE = "en_US.utf8"
		LC_CTYPE = "en_US.utf8"
		TABLESPACE = pg_default
		CONNECTION LIMIT = -1'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'twitter')\gexec
		
\connect twitter;

CREATE TABLE IF NOT EXISTS tweets (
    id Serial PRIMARY KEY,
    content text NOT NULL,
    username varchar(64) NOT NULL,
    timestamp TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS likes (
	username varchar(64) NOT NULL,
	timestamp TIMESTAMP NOT NULL,
	post_id int NOT NULL,
	CONSTRAINT post_id
      FOREIGN KEY(post_id) 
	  	REFERENCES tweets(id)
);

CREATE TABLE IF NOT EXISTS retweets (
	username varchar(64) NOT NULL,
	timestamp TIMESTAMP NOT NULL,
	post_id int NOT NULL,
	CONSTRAINT post_id
      FOREIGN KEY(post_id) 
	  	REFERENCES tweets(id)
);

