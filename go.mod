module github.com/tomer/twitter

go 1.14

require (
	github.com/fatih/structs v1.1.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.8.0
	go.uber.org/zap v1.16.0
)
