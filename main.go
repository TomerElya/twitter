package main

import (
	"github.com/tomer/twitter/twitter"
	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	server, err := twitter.NewServer(logger)
	if err != nil {
		panic(err)
	}
	server.Start()
}
