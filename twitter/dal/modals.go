package dal

import "time"

type tweet struct {
	Id        int64     `db:"id" json:"id"`
	Content   string    `db:"content" json:"content"`
	Username  string    `db:"username" json:"username"`
	Timestamp time.Time `db:"timestamp" json:"timestamp"`
	Likes     int       `db:"likes" json:"likes_count" mapstructure:"likes_count"`
	Retweets  int       `db:"retweets" json:"retweets_count" mapstructure:"retweets_count"`
}

type retweet struct {
	Id          int64     `db:"id" json:"tweet_id"`
	Content     string    `db:"content" json:"content"`
	RetweetUser string    `db:"retweet_user" json:"retweet_user" mapstructure:"retweet_user"`
	TweetUser   string    `db:"tweet_user" json:"tweet_user" mapstructure:"tweet_user"`
	Timestamp   time.Time `db:"timestamp" json:"timestamp"`
}

type TweetSubmission struct {
	Content  string `json:"content"`
	Username string `json:"username"`
}

// TweetActionSubmission is a singular action performed by a user on a post.
// i.e. like/retweet.
type TweetActionSubmission struct {
	PostId   int64  `json:"post_id" mapstructure:"post_id"`
	Username string `json:"username"`
}

func NewTweetActionSubmission(postId int64, username string) TweetActionSubmission {
	return TweetActionSubmission{
		PostId:   postId,
		Username: username,
	}
}
