package dal

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"os"
)

type ErrEnvVarNotFound struct {
	EnvVar string
}

func (err ErrEnvVarNotFound) Error() string {
	return fmt.Sprintf("Enviroment variable %s was not found or is empty",
		err.EnvVar)
}

const (
	dbTypeEnvar     = "DB_TYPE"
	connStringEnvar = "CONNECTION_STRING"
)

type DbProvider struct {
	db *sqlx.DB
}

func NewDbProvider() (DbProvider, error) {
	db, err := createDbConnection()
	if err != nil {
		return DbProvider{},  err
	}
	return DbProvider{
		db: db,
	}, nil
}

func (p *DbProvider) GetTweets() (tweets []tweet, err error) {
	err = p.db.Select(&tweets, GetTweets)
	return
}

func (p *DbProvider) PostTweet(tweetSubmission TweetSubmission) error {
	_, err := p.db.NamedExec(PostTweet, structs.Map(tweetSubmission))
	return err
}

func (p *DbProvider) PostLike(tweetAction TweetActionSubmission) error {
	_, err := p.db.NamedExec(PostLike, structs.Map(tweetAction))
	return err
}

func (p *DbProvider) PostRetweet(tweetAction TweetActionSubmission) error {
	_, err := p.db.NamedExec(PostRetweet, structs.Map(tweetAction))
	return err
}

func (p *DbProvider) GetRetweets() (retweets []retweet, err error) {
	err = p.db.Select(&retweets, GetRetweets)
	return
}

// createDbConnection parses the DB type (Postgres, mySql etc..)
// and the connection string to it from system environment variables and
// attempts to create a connection.
func createDbConnection() (*sqlx.DB, error) {
	dbType := os.Getenv(dbTypeEnvar)
	if dbType == "" {
		return nil, ErrEnvVarNotFound{EnvVar: dbTypeEnvar}
	}
	connString := os.Getenv(connStringEnvar)
	if connString == "" {
		return nil, ErrEnvVarNotFound{EnvVar: connStringEnvar}
	}

	db, err := sqlx.Open(dbType, connString)
	if err != nil {
		return &sqlx.DB{}, err
	}
	return db, db.Ping()
}
