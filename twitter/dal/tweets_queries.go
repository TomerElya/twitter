package dal

const PostTweet = `INSERT INTO tweets (content, username, timestamp) 
					 VALUES (:Content, :Username, current_timestamp);`

const GetTweets = `SELECT tweets.*,
(SELECT Count(*) FROM likes where post_id=tweets.id) as likes,
(SELECT Count(*) FROM retweets where post_id=tweets.id) as retweets
from tweets
GROUP BY id;`

const PostLike = `INSERT INTO likes (username, post_id,timestamp) VALUES (:Username, :PostId, current_timestamp);`

const PostRetweet = `INSERT INTO retweets (username, post_id,timestamp) VALUES (:Username, :PostId, current_timestamp);`

const GetRetweets = `SELECT tweets.id, 
retweets.username as retweet_user,
tweets.username as tweet_user,
tweets.content, 
retweets.timestamp
FROM retweets inner join tweets on retweets.post_id = tweets.id;`
