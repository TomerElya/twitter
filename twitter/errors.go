package twitter

import "fmt"

type ErrInvalidPostId struct{}

func (err ErrInvalidPostId) Error() string {
	return fmt.Sprintf("Failed parsing post id from url")
}

type ErrQueryParamNotFound struct {
	param string
}

func (err ErrQueryParamNotFound) Error() string {
	return fmt.Sprintf("Query param %s was not found", err.param)
}

type ErrEnvVarNotFound struct {
	EnvVar string
}

func (err ErrEnvVarNotFound) Error() string {
	return fmt.Sprintf("Enviroment variable %s was not found or is empty",
		err.EnvVar)
}
