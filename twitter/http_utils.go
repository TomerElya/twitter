package twitter

import (
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
	"net/url"
)

func sendOkJsonResponse(response interface{}, w http.ResponseWriter,
	logger *zap.Logger) {
	responseBytes, err := json.Marshal(response)
	if err != nil {
		logger.Error("Failed marshaling ok response body to JSON",
			zap.Error(err))
		return
	}
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(responseBytes)
	if err != nil {
		logger.Error("Failed writing ok response to client", zap.Error(err))
	}
}

func sendOkResponse(w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
}

func sendMessagedResponse(msg string, code int, w http.ResponseWriter,
	logger *zap.Logger) {
	w.WriteHeader(code)
	_, err := w.Write([]byte(msg))
	if err != nil {
		logger.Error("Failed sending response to client",
			zap.Error(err), zap.Int("code", code))
	}
}

func parseQueryParam(values url.Values, param string) (string, error) {
	value, ok := values[param]
	if !ok || len(values) == 0 {
		return "", ErrQueryParamNotFound{param}
	}
	return value[0], nil
}
