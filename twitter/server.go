package twitter

import (
	"github.com/gorilla/mux"
	"github.com/tomer/twitter/twitter/dal"
	"go.uber.org/zap"
	"net/http"
	"os"
)

const (
	addressEnvar = "SERVER_ADDRESS"
)

type internalRouter interface {
	initRouter(router *mux.Router)
}

type Server struct {
	address string

	internalRouters []internalRouter
	router          *mux.Router

	dbProvider dal.DbProvider

	logger *zap.Logger
}

func NewServer(logger *zap.Logger) (Server, error) {
	dbProvider, err := dal.NewDbProvider()
	if err != nil {
		return Server{}, err
	}

	address := os.Getenv(addressEnvar)
	if address == "" {
		return Server{}, ErrEnvVarNotFound{addressEnvar}
	}

	server := Server{
		address: address,

		router: mux.NewRouter(),

		dbProvider: dbProvider,

		logger: logger,
	}
	server.initialize()
	return server, nil
}

func (s *Server) initialize() {
	s.internalRouters = []internalRouter{
		newTweetsRouter(s.dbProvider, s.logger),
	}

	for _, internalRouter := range s.internalRouters {
		internalRouter.initRouter(s.router)
	}
}

// Start is a blocking function and must run in its own goroutine.
func (s *Server) Start() {
	s.logger.Info("Starting twitter server", zap.String("address", s.address))
	err := http.ListenAndServe(s.address, s.router)
	if err != nil {
		s.logger.Fatal("Failed to start twitter server", zap.Error(err))
	}
}
