package twitter

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/tomer/twitter/twitter/dal"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

const (
	postIdQueryParam = "id"
)

type tweetsRouter struct {
	dbHandler dal.DbProvider

	logger *zap.Logger
}

func newTweetsRouter(handler dal.DbProvider, logger *zap.Logger) internalRouter {
	router := tweetsRouter{
		dbHandler: handler,

		logger: logger,
	}
	return &router
}

func (r *tweetsRouter) initRouter(router *mux.Router) {
	router.HandleFunc("/tweets", r.handleGetTweets).Methods(http.MethodGet)
	router.HandleFunc("/tweets", r.handlePostTweet).Methods(http.MethodPost)

	router.HandleFunc(fmt.Sprintf("/tweets/{%s}/likes", postIdQueryParam),
		r.handlePostTweetLike).Methods(http.MethodPost)

	router.HandleFunc(fmt.Sprintf("/tweets/{%s}/retweet", postIdQueryParam),
		r.handlePostRetweet).Methods(http.MethodPost)
	router.HandleFunc("/retweets", r.handleGetRetweets).Methods(http.
		MethodGet)
}

func (r *tweetsRouter) handleGetTweets(res http.ResponseWriter,
	req *http.Request) {
	tweets, err := r.dbHandler.GetTweets()
	if err != nil {
		r.logger.Error("Failed fetching tweets from database", zap.Error(err))
		sendMessagedResponse("Failed fetching tweets	",
			http.StatusInternalServerError, res, r.logger)
		return
	}
	sendOkJsonResponse(tweets, res, r.logger)
}

func (r *tweetsRouter) handlePostTweet(res http.ResponseWriter, req *http.Request) {
	tweetSubmission, err := r.parseTweetRequest(req)
	if err != nil {
		sendMessagedResponse("Failed parsing tweet submission",
			http.StatusBadRequest, res, r.logger)
		return
	}

	err = r.dbHandler.PostTweet(tweetSubmission)
	if err != nil {
		r.logger.Error("Failed inserting tweet to database", zap.Error(err))
		sendMessagedResponse("Failed inserting tweet to database",
			http.StatusInternalServerError, res, r.logger)
		return
	}

	sendOkResponse(res)
}

func (r *tweetsRouter) handlePostTweetLike(res http.ResponseWriter, req *http.Request) {
	tweetAction, err := r.parseUserTweetAction(req)
	if err != nil {
		sendMessagedResponse("Failed parsing like request",
			http.StatusBadRequest, res, r.logger)
		return
	}

	err = r.dbHandler.PostLike(tweetAction)
	if err != nil {
		r.logger.Error("Failed inserting like to database", zap.Error(err))
		sendMessagedResponse("Failed inserting like to database",
			http.StatusInternalServerError, res, r.logger)
		return
	}

	sendOkResponse(res)
}

func (r *tweetsRouter) handlePostRetweet(res http.ResponseWriter, req *http.Request) {
	tweetAction, err := r.parseUserTweetAction(req)
	if err != nil {
		sendMessagedResponse("Failed parsing like request",
			http.StatusBadRequest, res, r.logger)
		return
	}

	err = r.dbHandler.PostRetweet(tweetAction)
	if err != nil {
		r.logger.Error("Failed inserting like to database", zap.Error(err))
		sendMessagedResponse("Failed inserting like to database",
			http.StatusInternalServerError, res, r.logger)
		return
	}

	sendOkResponse(res)
}

func (r *tweetsRouter) handleGetRetweets(res http.ResponseWriter, req *http.Request) {
	retweets, err := r.dbHandler.GetRetweets()
	if err != nil {
		r.logger.Error("Failed fetching retweets from database", zap.Error(err))
		sendMessagedResponse("Failed fetching retweets",
			http.StatusInternalServerError, res, r.logger)
		return
	}
	sendOkJsonResponse(retweets, res, r.logger)
}

func (r *tweetsRouter) parseUserTweetAction(req *http.Request) (dal.
	TweetActionSubmission, error) {
	postIdString := mux.Vars(req)[postIdQueryParam]
	if postIdString == "" {
		r.logger.Error("Failed parsing post id from url", zap.String("url",
			req.URL.RawPath))
		return dal.TweetActionSubmission{}, ErrInvalidPostId{}
	}

	postId, err := strconv.ParseInt(postIdString, 10, 64)
	if err != nil {
		r.logger.Error("Failed converting post id variable to int64",
			zap.String("postid", postIdString), zap.Error(err))
		return dal.TweetActionSubmission{}, err
	}

	type usernameJson struct {
		Username string `json:"username"`
	}
	var username usernameJson
	err = json.NewDecoder(req.Body).Decode(&username)
	if err != nil {
		r.logger.Error("Failed decoding request body to username",
			zap.Error(err))
		return dal.TweetActionSubmission{}, nil
	}

	return dal.NewTweetActionSubmission(postId, username.Username), nil
}

func (r *tweetsRouter) parseTweetRequest(req *http.Request) (
	tweetSubmission dal.TweetSubmission, err error) {
	err = json.NewDecoder(req.Body).Decode(&tweetSubmission)
	if err != nil {
		r.logger.Error("Failed decoding request body to tweet submission",
			zap.Error(err))
		return dal.TweetSubmission{}, nil
	}
	return
}
